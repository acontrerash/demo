package com.example.demo;

import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@Configuration
public class SecurityConfiguration extends WebSecurityConfigurerAdapter{
	
	@Override  
    public void configure(HttpSecurity http) throws Exception {  
        http  
            .antMatcher("/**")
            .authorizeRequests()
            .antMatchers(HttpMethod.GET, "/", "/api/**", "/login-form-custom**").permitAll()
            .anyRequest().authenticated()
            .and()
            	.logout().logoutSuccessUrl("/")
            .and()
            	.oauth2Client()
            .and()
	            .oauth2Login().redirectionEndpoint()
	            .baseUri("/authorization-code/callback*");
    }
	
	@Override
	public void configure(WebSecurity web) throws Exception {
	    web
	    .ignoring()
	    .antMatchers(HttpMethod.POST, "/api/**")
	    .antMatchers(HttpMethod.DELETE, "/api/**")
	    .antMatchers(HttpMethod.PUT, "/api/**");
	}
}

package com.example.demo.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import com.okta.sdk.authc.credentials.TokenClientCredentials;
import com.okta.sdk.client.Client;
import com.okta.sdk.client.Clients;
import com.okta.sdk.resource.ResourceException;
import com.okta.sdk.resource.user.User;
import com.okta.sdk.resource.user.UserBuilder;
import com.okta.sdk.resource.user.UserList;

@RestController
@RequestMapping("/api/v1/users")
public class UserController {
	
	@Value("${app.domain}")
	private String appDomain;
	
	@Value("${app.okta.token}")
	private String token;
	
	@Bean
	public Client client() {
		Client clientConfig = Clients.builder()
				.setOrgUrl(appDomain)
				.setClientCredentials(new TokenClientCredentials(token))
				.build();
		
		return clientConfig;
	}
	
	@GetMapping("/all")
	private UserList getAllUsers() {
		UserList list = client().listUsers();
				
		return list;
	}
	
	@GetMapping("/{id}")
	public User getUser(@PathVariable(required = true) String id) {
		User user = null;
		try {			
			user = client().getUser(id);
			return user;
		}catch (ResourceException e) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, "User not found!");
		}
	}
	
	@PostMapping("/add")
	public User addUser(@RequestBody com.example.demo.bean.User user){
		User newUser = UserBuilder.instance()
				.setEmail(user.getEmail())
				.setFirstName(user.getFirstName())
				.setLastName(user.getLastName())
				.buildAndCreate(client());	
		return newUser;
	}
	
	@PutMapping("/update/{id}")
	public User updateUser(@RequestBody com.example.demo.bean.User user, @PathVariable(required = true) String id) {
		
		User userToUpdate = client().getUser(id);
		
		userToUpdate.getProfile().setFirstName(user.getFirstName());
		userToUpdate.getProfile().setLastName(user.getLastName());
		userToUpdate.getProfile().setEmail(user.getEmail());
		
		userToUpdate.update();
		
		return userToUpdate;
	}
	
	@DeleteMapping("/delete/{id}")
	public void deleteUser(@PathVariable(required = true) String id) {
		User user = client().getUser(id);
		user.deactivate();
		user.delete();
	}
}

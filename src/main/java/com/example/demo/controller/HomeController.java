package com.example.demo.controller;

import java.net.MalformedURLException;
import java.net.URL;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.okta.spring.boot.oauth.config.OktaOAuth2Properties;

@Controller
public class HomeController {
	
	private static final String STATE = "state";
    private static final String NONCE = "nonce";
    private static final String SCOPES = "scopes";
    private static final String OKTA_BASE_URL = "oktaBaseUrl";
    private static final String OKTA_CLIENT_ID = "oktaClientId";
    private static final String REDIRECT_URI = "redirectUri";
    private static final String ISSUER_URI = "issuerUri";
	
	private final OktaOAuth2Properties oktaOAuth2Properties;
		
	HomeController(OktaOAuth2Properties oktaOAuth2Properties){
		this.oktaOAuth2Properties = oktaOAuth2Properties;
	}
	
	@GetMapping("/")  
    public String index() {  
        return "index.html";  
    }
        
    @GetMapping(value = "/login-form-custom")
    public ModelAndView login(HttpServletRequest request,
                              @RequestParam(name = "state", required = false) String state,
                              @RequestParam(name = "nonce") String nonce) throws MalformedURLException {

    	if (state == null) {
            return new ModelAndView("redirect:" + oktaOAuth2Properties.getRedirectUri());
        }

        String issuer = oktaOAuth2Properties.getIssuer();
        String orgUrl = new URL(new URL(issuer), "/").toString();

        ModelAndView mav = new ModelAndView("login");
        mav.addObject(STATE, state);
        mav.addObject(NONCE, nonce);
        mav.addObject(SCOPES, oktaOAuth2Properties.getScopes());
        mav.addObject(OKTA_BASE_URL, orgUrl);
        mav.addObject(OKTA_CLIENT_ID, oktaOAuth2Properties.getClientId());
        mav.addObject(REDIRECT_URI,
            request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() +
            request.getContextPath() + "/authorization-code/callback"
        );
        mav.addObject(ISSUER_URI, issuer);

        return mav;
    }
    
    
	
}
